package com.staticdot.hangman;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.staticdot.hangman.cdi.BeanRepository;
import com.staticdot.hangman.logic.Game;
import com.staticdot.hangman.logic.GameListener;
import com.staticdot.hangman.view.DoodlePanel;
import com.staticdot.hangman.view.Keyboard;
import com.staticdot.hangman.view.KeyboardListener;
import com.staticdot.hangman.view.WordPanel;
import com.staticdot.hangman.view.layout.LandscapeLayoutStrategy;
import com.staticdot.hangman.view.layout.LayoutStrategy;
import com.staticdot.hangman.view.layout.PortraitLayoutStrategy;

public class MainGamePanel extends SurfaceView implements
		SurfaceHolder.Callback {

	private static final String TAG = MainGamePanel.class.getSimpleName();

	private final GameLoop gameLoop;
	private final Keyboard keyboard;
	private final WordPanel wordPanel;
	private final DoodlePanel doodlePanel;
	private final GameListener gameListener;
    private ExitListener exitListener;
	private final Game game = BeanRepository.getBean(Game.class);
	
	public MainGamePanel(Context context) {
		super(context);
		getHolder().addCallback(this);
		setFocusable(true);

		gameLoop = new GameLoop(getHolder(), this);
		keyboard = new Keyboard();
		keyboard.setListener(keyboardListener);
		wordPanel = new WordPanel();
		doodlePanel = new DoodlePanel();
		gameListener = new DefaultGameListener(this);
		game.setListener(gameListener);

        refreshGameState();
	}

    public void setExitListener(ExitListener exitListener) {
        this.exitListener = exitListener;
    }

    ExitListener getExitListener() {
        return exitListener;
    }

	void refreshGameState() {
		wordPanel.setWord(game.getHiddenWord());
		keyboard.refresh();
	}

	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		Log.i(TAG, "surface changed: " + width + "x" + height);
		LayoutStrategy layoutStrategy;
		if (width < height) {
			layoutStrategy = new PortraitLayoutStrategy(keyboard);
		} else {
			layoutStrategy = new LandscapeLayoutStrategy(keyboard);
		}
		keyboard.setup(layoutStrategy.setupKeyboardRect(width, height));
		doodlePanel.setup(layoutStrategy.setupDoodleRect(width, height));
		wordPanel.setup(layoutStrategy.setupWordRect(width, height));
	}

	public void surfaceCreated(SurfaceHolder holder) {
		Log.d(TAG, "Surface created");
		gameLoop.start();
	}

	public void surfaceDestroyed(SurfaceHolder holder) {
		Log.d(TAG, "Surface destroyed");
		gameLoop.stop();
	}

	public void render(Canvas canvas) {
		canvas.drawColor(Color.BLACK);

		keyboard.draw(canvas);
		wordPanel.draw(canvas);
		doodlePanel.draw(canvas);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			keyboard.onTouch((int) event.getX(), (int) event.getY());
		}
		return super.onTouchEvent(event);
	}

	private final KeyboardListener keyboardListener = new KeyboardListener() {

		public void keyPressed(char key) {
			guess(key);
		}
	};

	/**
	 * @return true if the event was handled
	 */
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode >= KeyEvent.KEYCODE_A && keyCode <= KeyEvent.KEYCODE_Z) {
			char unicodeChar = (char) (keyCode - KeyEvent.KEYCODE_A + 'A');
			guess(unicodeChar);
			return true;
		}
		return false;
	}

	private void guess(char key) {
		game.guess(key);
		keyboard.disable(key);
	}
}
