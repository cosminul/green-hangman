package com.staticdot.hangman;

public interface ExitListener {

    void onExit();
}
