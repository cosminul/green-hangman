package com.staticdot.hangman;

import java.util.concurrent.TimeUnit;

import android.graphics.Canvas;
import android.util.Log;
import android.view.SurfaceHolder;

public class GameLoop implements Runnable {

	private static final String TAG = GameLoop.class.getSimpleName();

	/** The number of frames per second */
	private static final int FRAMES_PER_SECOND = 20;
	/** How long a frame should take */
	private static final int PERIOD = 1000 / FRAMES_PER_SECOND;
	/**
	 * Number of frames with a delay of 0 ms before the animation thread yields
	 * to other running threads
	 */
	private static final int NO_DELAYS_PER_YIELD = 16;
	/**
	 * Number of frames that can be skipped in one animation loop i.e. the games
	 * state is updated but not rendered
	 */
	private static final int MAX_FRAME_SKIPS = 5;

	private boolean running;

	private final SurfaceHolder surfaceHolder;
	private final MainGamePanel gamePanel;

	private Thread thread;

	public GameLoop(SurfaceHolder surfaceHolder, MainGamePanel gamePanel) {
		super();
		this.surfaceHolder = surfaceHolder;
		this.gamePanel = gamePanel;
	}

	public void start() {
		running = true;
		thread = new Thread(this);
		thread.start();
	}

	public void stop() {
		if (!running) {
			throw new IllegalStateException("Loop is not running.");
		}
		running = false;
		boolean retry = true;
		while (retry) {
			try {
                Log.d(TAG, "Stopping game...");
				thread.join();
				retry = false;
			} catch (InterruptedException e) {
				Log.w(TAG, "Failed to shut down the thread, retrying.", e);
			}
		}
	}

	public void run() {
		Log.d(TAG, "Starting game loop");
		long overSleepTime = 0;
		long excess = 0;
		long noDelays = 0;
		while (running) {
			long beforeTime = getTicks();

			handleEvents();
			updateGameState();
			paintScreen();

			long afterTime = getTicks();
			long timeDiff = afterTime - beforeTime;
			Log.d(TAG, "timeDiff was " + timeDiff + " ms");
			long sleepTime = (PERIOD - timeDiff) - overSleepTime; // time left in
																// this loop
			if (sleepTime > 0) {
				// Some time left in this loop
				delay(sleepTime);
				overSleepTime = (getTicks() - afterTime) - sleepTime;
			} else {
				// frame took longer than the period
				excess -= sleepTime; // store excess time value
				overSleepTime = 0;
				if (++noDelays >= NO_DELAYS_PER_YIELD) {
					// Give another thread a chance to run
					delay(1); // instead of thread yielding
					noDelays = 0;
				}
			}
			/*
			 * If frame animation is taking too long, update the game state
			 * without rendering it, to get the updates/sec nearer to the
			 * required FPS.
			 */
			int skips = 0;
			while ((excess > PERIOD) && (skips < MAX_FRAME_SKIPS)) {
				excess -= PERIOD;
				updateGameState(); // update state but don't render
				skips++;
			}
			Log.d(TAG, "Skipped" + skips + " frames.");
		}
	}

	private long getTicks() {
		return System.currentTimeMillis();
	}

	private void delay(long milliseconds) {
		try {
			TimeUnit.MILLISECONDS.sleep(milliseconds);
		} catch (InterruptedException e) {
			Log.w(TAG, e);
		}
	}

	private void handleEvents() {
	}

	private void updateGameState() {
	}

	private void paintScreen() {
		Canvas canvas = null;
		// try locking the canvas for exclusive pixel editing on the surface
		try {
			canvas = surfaceHolder.lockCanvas();
			synchronized (surfaceHolder) {
				gamePanel.render(canvas);
				TimeUnit.MILLISECONDS.sleep(200);
			}
		} catch (InterruptedException e) {
			Log.w(TAG, e);
		} finally {
			// in case of an exception the surface is not left in
			// an inconsistent state
			if (canvas != null) {
				surfaceHolder.unlockCanvasAndPost(canvas);
			}
		}
	}
}
