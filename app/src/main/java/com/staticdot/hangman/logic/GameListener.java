package com.staticdot.hangman.logic;

public interface GameListener {

    void gameStarted();

	void gameWon();

	void gameLost();
}
