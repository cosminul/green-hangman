package com.staticdot.hangman.logic;

import java.util.HashSet;
import java.util.Set;

import com.staticdot.hangman.model.HardcodedStringListSource;
import com.staticdot.hangman.model.HiddenWord;
import com.staticdot.hangman.model.RandomWordFactory;
import com.staticdot.hangman.model.WordFactory;

public class Game {

	private HiddenWord hiddenWord;
	private int failCount;
	private final Set<Character> attempts;
	private WordFactory wordFactory;
	private GameListener gameListener;

	public Game() {
		attempts = new HashSet<Character>();
        wordFactory = new RandomWordFactory(new HardcodedStringListSource());
		reset();
	}

	public void reset() {
		attempts.clear();
		initHiddenWord();
		failCount = 0;

        if(gameListener != null) {
            gameListener.gameStarted();
        }
	}

	private void initHiddenWord() {
		String word = wordFactory.getWord();
		hiddenWord = new HiddenWord(word);
		// reveal first letter
		char firstLetter = word.charAt(0);
		this.hiddenWord.guess(firstLetter);
		attempts.add(firstLetter);
	}

	public HiddenWord getHiddenWord() {
		return hiddenWord;
	}

	public int getFailCount() {
		return failCount;
	}

	private void fail() {
		failCount++;
	}

	public void guess(char letter) {
		// If letter has not been tried before
		if (!letterHasBeenTried(letter)) {
			// Mark it as tried
			attempts.add(letter);
			// See if it's contained in the hidden word
			if (!hiddenWord.guess(letter)) {
				fail();
			}
			decideGameOver();
		}
	}

	public boolean letterHasBeenTried(char letter) {
		return attempts.contains(letter);
	}

	private void decideGameOver() {
		if (hiddenWord.isGuessed()) {
			gameListener.gameWon();
		} else if (failCount >= 6) {
			gameListener.gameLost();
		}
	}

	public void setListener(GameListener listener) {
		gameListener = listener;
	}

    public void setWordFactory(WordFactory wordFactory) {
        this.wordFactory = wordFactory;
    }
}
