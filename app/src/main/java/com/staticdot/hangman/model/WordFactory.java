package com.staticdot.hangman.model;

public interface WordFactory {

	String getWord();
}
