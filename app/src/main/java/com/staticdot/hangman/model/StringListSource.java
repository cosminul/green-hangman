package com.staticdot.hangman.model;

import java.util.List;

public interface StringListSource {

    List<String> getStrings();
}
