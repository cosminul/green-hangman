package com.staticdot.hangman.model;

import java.util.Arrays;
import java.util.List;

public class HardcodedStringListSource implements StringListSource {

    // 9 letters at most, please!
    private static final List<String> WORDS = Arrays.asList(
            "Antelope",
            "Butterfly",
            "Cormorant",
            "Dolphin",
            "Elephant",
            "Flamingo",
            "Groundhog",
            "Hedgehog",
            "Iguana",
            "Jaguar",
            "Kangaroo",
            "Leopard",
            "Macaque",
            "Nighthawk",
            "Opossum",
            "Platypus",
            "Quail",
            "Raccoon",
            "Squirrel",
            "Tortoise",
            "Vulture",
            "Wombat",
            "Zebra"
    );

    public List<String> getStrings() {
        return WORDS;
    }
}
