package com.staticdot.hangman.model;

import java.util.Locale;

import android.util.Log;

public class HiddenWord {

	private static final String TAG = HiddenWord.class.getSimpleName();

	private final char[] letters;
	private final boolean[] revealed;

	public HiddenWord(String word) {
		letters = word.toUpperCase(Locale.US).toCharArray();
		revealed = new boolean[letters.length];
	}

	public boolean guess(char letter) {
		boolean guessed = false;
		for (int i = 0; i < letters.length; i++) {
			if (letters[i] == Character.toUpperCase(letter)) {
				revealed[i] = true;
				guessed = true;
				Log.d(TAG, "guessed " + letter);
			}
		}
		return guessed;
	}

	public boolean isGuessed() {
		for (int i = 0; i < revealed.length; i++) {
			if (revealed[i] == false) {
				return false;
			}
		}
		return true;
	}

	public String reveal() {
		return new String(letters);
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < letters.length; i++) {
			if (revealed[i]) {
				sb.append(letters[i]);
			} else {
				sb.append('_');
			}
			sb.append(' ');
		}
		return sb.toString();
	}
}
