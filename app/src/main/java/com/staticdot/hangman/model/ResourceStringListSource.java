package com.staticdot.hangman.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

public class ResourceStringListSource implements StringListSource {

    private final InputStream inputStream;

    public ResourceStringListSource(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public List<String> getStrings() {

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        while(true) {
            String line = null;
            try {
                line = bufferedReader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(line == null) {
                break;
            }

        }
        return null;
    }
}
