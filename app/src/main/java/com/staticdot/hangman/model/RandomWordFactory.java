package com.staticdot.hangman.model;

import java.util.List;
import java.util.Random;

public class RandomWordFactory implements WordFactory {

    private final Random random = new Random();
    private final List<String> words;

    public RandomWordFactory(StringListSource stringListSource) {
        words = stringListSource.getStrings();
    }

    public String getWord() {
        int randomIndex = random.nextInt(words.size());
        return words.get(randomIndex);
    }
}
