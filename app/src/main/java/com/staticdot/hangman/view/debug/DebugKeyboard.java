package com.staticdot.hangman.view.debug;

import com.staticdot.hangman.view.Keyboard;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;

public class DebugKeyboard extends Keyboard {

	private final Paint debugPaint;

	public DebugKeyboard() {
		debugPaint = new Paint();
		debugPaint.setColor(Color.MAGENTA);
		debugPaint.setStyle(Style.STROKE);
	}

	@Override
	public void draw(Canvas canvas) {
		canvas.drawRect(rect.left, rect.top, rect.right - 1, rect.bottom - 1,
				debugPaint);
		super.draw(canvas);
	}
}
