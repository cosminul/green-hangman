package com.staticdot.hangman.view.layout;

import android.graphics.Rect;

public interface LayoutStrategy {

	Rect setupKeyboardRect(int surfaceWidth, int surfaceHeight);

	Rect setupDoodleRect(int surfaceWidth, int surfaceHeight);

	Rect setupWordRect(int surfaceWidth, int surfaceHeight);
}
