package com.staticdot.hangman.view;

import com.staticdot.hangman.cdi.BeanRepository;
import com.staticdot.hangman.logic.Game;

import android.graphics.Canvas;
import android.graphics.Rect;

public class Keyboard implements ScreenElement {

	private static final float ASPECT_RATIO = 4.f / 7.f;

	private final char[] characters;
	private final char startCharacter = 'A';
	private final char endCharacter = 'Z';
	private final Key[] keys;
	private KeyboardListener listener;
	private int maxKeysPerRow;
	private final int maxKeysPerShortestSide = 7;
	protected Rect rect;
	private int keySpace;

	public Keyboard() {
		characters = new char[endCharacter - startCharacter + 1];
		for (char c = startCharacter; c <= endCharacter; c++) {
			characters[c - startCharacter] = c;
		}
		keys = new Key[characters.length];
		for (int i = 0; i < characters.length; i++) {
			keys[i] = new Key(characters[i]);
		}
		refresh();
		rect = new Rect();
	}

	public float getAspectRatio() {
		return ASPECT_RATIO;
	}

	public void refresh() {
		for (int i = 0; i < characters.length; i++) {
			// TODO the keyboard has nothing to do with the game instance
			// It is the game that has to disable the keyboard
			Game game = BeanRepository.getBean(Game.class);
			if (game.letterHasBeenTried(characters[i])) {
				keys[i].setEnabled(false);
			} else {
				keys[i].setEnabled(true);
			}
		}
	}

	private void computeKeySpace(Rect rect) {
		int rectWidth = rect.right - rect.left;
		int rectHeight = rect.bottom - rect.top;
		int widestSide = Math.max(rectWidth, rectHeight);
		keySpace = widestSide / maxKeysPerShortestSide;
		if (rectWidth > rectHeight) {
			maxKeysPerRow = maxKeysPerShortestSide;
		} else {
			maxKeysPerRow = (int) Math.ceil((double) keys.length
					/ (double) maxKeysPerShortestSide);
		}
	}

	public void setup(Rect rect) {
		computeKeySpace(rect);
		this.rect = rect;
		int yPos = 0;
		int margin = keySpace / 10;
		int size = keySpace - (2 * margin);

		int col = 0;
		for (int i = 0; i < keys.length; i++) {
			int x = rect.left + (col * keySpace) + margin;
			int y = rect.top + yPos + margin;

			Rect keyBox = new Rect(x, y, x + size, y + size);
			keys[i].setup(keyBox);

			col++;
			if (col == maxKeysPerRow) {
				int keysLeft = keys.length - (i + 1);
				if (keysLeft < maxKeysPerRow) {
					col = (maxKeysPerRow - keysLeft) / 2;
				} else {
					col = 0;
				}
				yPos += keySpace;
			}
		}
	}

	public void draw(Canvas canvas) {
		for (Key key : keys) {
			key.draw(canvas);
		}
	}

	public void disable(char letter) {
		for (Key key : keys) {
			if (Character.toUpperCase(key.getChar()) == Character
					.toUpperCase(letter)) {
				key.setEnabled(false);
				break;
			}
		}
	}

	public void onTouch(int x, int y) {
		for (Key key : keys) {
			if (key.getBox().contains(x, y)) {
				if (key.isEnabled()) {
					key.onTouch(x, y);
					notifyPress(key.getChar());
					break;
				}
			}
		}
	}

	private void notifyPress(char key) {
		listener.keyPressed(key);
	}

	public void setListener(KeyboardListener listener) {
		this.listener = listener;
	}
}
