package com.staticdot.hangman.view;

import android.graphics.Canvas;
import android.graphics.Rect;

public interface ScreenElement {

	void setup(Rect rect);

	void draw(Canvas canvas);
}
