package com.staticdot.hangman.view.layout;

import android.graphics.Rect;

import com.staticdot.hangman.view.Keyboard;

public class LandscapeLayoutStrategy implements LayoutStrategy {

	private final Keyboard keyboard;

	public LandscapeLayoutStrategy(Keyboard keyboard) {
		this.keyboard = keyboard;
	}

	public Rect setupKeyboardRect(int surfaceWidth, int surfaceHeight) {
		Rect rect = new Rect();
		int keyboardWidth = (int) (surfaceHeight * keyboard.getAspectRatio());
		rect.top = 0;
		rect.bottom = surfaceHeight;
		rect.left = surfaceWidth - keyboardWidth;
		rect.right = keyboardWidth;
		return rect;
	}

	public Rect setupDoodleRect(int surfaceWidth, int surfaceHeight) {
		Rect rect = new Rect();
		int keyboardWidth = (int) (surfaceHeight * keyboard.getAspectRatio());
		rect.top = 0;
		rect.bottom = surfaceHeight - 50;
		rect.left = 0;
		rect.right = surfaceWidth - keyboardWidth;
		return rect;
	}

	public Rect setupWordRect(int surfaceWidth, int surfaceHeight) {
		Rect rect = new Rect();
		int keyboardWidth = (int) (surfaceHeight * keyboard.getAspectRatio());
		rect.top = surfaceHeight - 40;
		rect.bottom = surfaceHeight;
		rect.left = 0;
		rect.right = surfaceWidth - keyboardWidth;
		return rect;
	}
}
