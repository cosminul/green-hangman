package com.staticdot.hangman.view.debug;

import com.staticdot.hangman.view.WordPanel;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;

public class DebugWordPanel extends WordPanel {

	private final Paint debugPaint;

	public DebugWordPanel() {
		debugPaint = new Paint();
		debugPaint.setColor(Color.MAGENTA);
		debugPaint.setStyle(Style.STROKE);
	}

	@Override
	public void draw(Canvas canvas) {
		canvas.drawRect(rect.left, rect.top, rect.right - 1, rect.bottom - 1,
				debugPaint);
		super.draw(canvas);
	}
}
