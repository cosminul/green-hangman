package com.staticdot.hangman.view.debug;

import com.staticdot.hangman.view.DoodlePanel;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

public class DebugDoodlePanel extends DoodlePanel {

	private final Paint debugPaint;

	public DebugDoodlePanel() {
		debugPaint = new Paint();
		debugPaint.setColor(Color.MAGENTA);
		debugPaint.setStyle(Paint.Style.STROKE);
		debugPaint.setStrokeWidth(1);
	}

	@Override
	public void draw(Canvas canvas) {
		canvas.drawRect(doodleLeft, doodleTop, doodleLeft + doodleWidth - 1,
				doodleTop + doodleHeight - 1, debugPaint);
		super.draw(canvas);
	}
}
