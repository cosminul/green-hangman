package com.staticdot.hangman.view.layout;

import android.graphics.Rect;

import com.staticdot.hangman.view.Keyboard;

public class PortraitLayoutStrategy implements LayoutStrategy {

	private final Keyboard keyboard;

	public PortraitLayoutStrategy(Keyboard keyboard) {
		this.keyboard = keyboard;
	}

	public Rect setupKeyboardRect(int surfaceWidth, int surfaceHeight) {
		Rect rect = new Rect();
		int keyboardHeight = (int) (surfaceWidth * keyboard.getAspectRatio());
		rect.top = surfaceHeight - keyboardHeight;
		rect.bottom = surfaceHeight;
		rect.left = 0;
		rect.right = surfaceWidth;
		return rect;
	}

	public Rect setupDoodleRect(int surfaceWidth, int surfaceHeight) {
		Rect rect = new Rect();
		int keyboardHeight = (int) (surfaceWidth * keyboard.getAspectRatio());
		rect.top = 0;
		rect.bottom = surfaceHeight - keyboardHeight - 50;
		rect.left = 0;
		rect.right = surfaceWidth;
		return rect;
	}

	public Rect setupWordRect(int surfaceWidth, int surfaceHeight) {
		Rect rect = new Rect();
		int keyboardHeight = (int) (surfaceWidth * keyboard.getAspectRatio());
		rect.top = surfaceHeight - keyboardHeight - 50;
		rect.bottom = surfaceHeight - keyboardHeight - 10;
		rect.left = 0;
		rect.right = surfaceWidth;
		return rect;
	}
}
