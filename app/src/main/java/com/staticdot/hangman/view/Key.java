package com.staticdot.hangman.view;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;

class Key implements ScreenElement {

	private static final String TAG = Key.class.getSimpleName();

	private Rect box;
	private final char k;
	private Paint bgPaint;
	private Paint disabledBgPaint;
	private Paint textPaint;
	private int labelWidth;
	private boolean enabled = true;

	public Key(char k) {
		box = new Rect();
		this.k = k;
		setupPaint();
	}

	private void setupPaint() {
		bgPaint = new Paint();
		bgPaint.setColor(Color.rgb(0, 192, 0)); // Green

		disabledBgPaint = new Paint();
		disabledBgPaint.setColor(Color.rgb(128, 128, 128)); // Light gray

		textPaint = new Paint();
		textPaint.setColor(Color.WHITE);
		textPaint.setTextSize(32);
	}

	public char getChar() {
		return k;
	}

	public Rect getBox() {
		return new Rect(box);
	}

	public void setup(Rect rect) {
		box = new Rect(rect);
		int keySize = rect.bottom - rect.top;
		textPaint.setTextSize(keySize * 0.8f);
		measureText();
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public void draw(Canvas canvas) {
		drawBox(canvas);
		drawText(canvas);
	}

	private void drawBox(Canvas canvas) {
		if (enabled) {
			canvas.drawRect(box, bgPaint);
		} else {
			canvas.drawRect(box, disabledBgPaint);
		}
	}

	private void drawText(Canvas canvas) {
		float textX = box.left + ((box.right - box.left - labelWidth) / 2.f);
		float textY = box.bottom - textPaint.descent();
		String label = Character.toString(k);
		canvas.drawText(label, textX, textY, textPaint);
	}

	private void measureText() {
		String text = Character.toString(k);
		Rect bounds = new Rect();
		textPaint.getTextBounds(text, 0, text.length(), bounds);
		labelWidth = bounds.right - bounds.left;
	}

	public void onTouch(int x, int y) {
		Log.d(TAG, "touch key " + k);
	}
}
