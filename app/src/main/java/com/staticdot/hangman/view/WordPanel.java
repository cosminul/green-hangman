package com.staticdot.hangman.view;

import com.staticdot.hangman.model.HiddenWord;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;

public class WordPanel implements ScreenElement {

	private HiddenWord word;
	private final Paint paint;
	protected Rect rect;
	private int textWidth;

	public WordPanel() {
		paint = new Paint();
		paint.setColor(Color.CYAN);
		paint.setTypeface(Typeface.MONOSPACE);
		rect = new Rect();
	}

	public void setWord(HiddenWord word) {
		this.word = word;
		measureText(rect);
	}

	public void setup(Rect rect) {
		this.rect = rect;
		paint.setTextSize((rect.bottom - rect.top) * 0.8f);
		measureText(rect);
	}

	public void draw(Canvas canvas) {
		float xPos = rect.left + ((rect.right - rect.left - textWidth) / 2.f);
		float baseLine = rect.bottom - paint.descent();
		canvas.drawText(word.toString(), xPos, baseLine, paint);
	}

	private void measureText(Rect rect) {
		String text = word.toString();
		Rect bounds = new Rect();
		paint.getTextBounds(text, 0, text.length(), bounds);
		textWidth = bounds.right - bounds.left;

		if (rect != null) {
			int rectWidth = rect.right - rect.left;
			while (textWidth > rectWidth && paint.getTextSize() > 1) {
				paint.setTextSize(paint.getTextSize() - 1);
				measureText(rect);
			}
		}
	}
}
