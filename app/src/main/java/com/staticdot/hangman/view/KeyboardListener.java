package com.staticdot.hangman.view;

public interface KeyboardListener {

	void keyPressed(char key);
}
