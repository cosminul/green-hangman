package com.staticdot.hangman.view;

import com.staticdot.hangman.cdi.BeanRepository;
import com.staticdot.hangman.logic.Game;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Join;
import android.graphics.Rect;

public class DoodlePanel implements ScreenElement {

	private final Paint paint;
	private final Game game = BeanRepository.getBean(Game.class);

	protected int doodleTop;
	protected int doodleHeight;
	protected int doodleWidth;
	protected int doodleLeft;
	private int doodleCenterX;
	private int headSize;

	public DoodlePanel() {
		paint = new Paint();
		paint.setColor(Color.GREEN);
		paint.setStyle(Paint.Style.STROKE);
		paint.setStrokeWidth(3);
		paint.setStrokeJoin(Join.ROUND);
		paint.setStrokeCap(Cap.ROUND);

		setup(new Rect());
	}

	public void setup(Rect rect) {
		doodleTop = rect.top;
		doodleHeight = rect.bottom - rect.top;
		doodleWidth = (int) (doodleHeight * (3.f / 4.f));
		doodleLeft = (rect.left + rect.right - doodleWidth) / 2;
		doodleCenterX = doodleLeft + (doodleWidth / 2);

		headSize = doodleHeight / 7;
	}

	public void draw(Canvas canvas) {
		drawSticks(canvas);
		drawMan(canvas);
	}

	private void drawSticks(Canvas canvas) {
        drawBigVerticalStick(canvas);
        drawHorizontalStick(canvas);
        drawSmallVerticalStick(canvas);
	}

    private void drawBigVerticalStick(Canvas canvas) {
        canvas.drawLine(doodleLeft + (doodleWidth * 0.1f), doodleTop
                + doodleHeight, doodleLeft, doodleTop, paint);
    }

    private void drawHorizontalStick(Canvas canvas) {
        canvas.drawLine(doodleLeft, doodleTop, doodleCenterX, doodleTop, paint);
    }

    private void drawSmallVerticalStick(Canvas canvas) {
        canvas.drawLine(doodleCenterX, doodleTop, doodleCenterX, doodleTop
                + (doodleHeight * 0.04f), paint);
    }

	private void drawMan(Canvas canvas) {
		drawHead(canvas);
		drawBody(canvas);
		drawLeftHand(canvas);
		drawRightHand(canvas);
		drawLeftLeg(canvas);
		drawRightLeg(canvas);
	}

	private void drawHead(Canvas canvas) {
		if (game.getFailCount() >= 1) {
			canvas.drawCircle(doodleCenterX, doodleTop + headSize,
					headSize / 2, paint);
		}
	}

	private void drawBody(Canvas canvas) {
		if (game.getFailCount() >= 2) {
			canvas.drawLine(doodleCenterX, doodleTop + (1.5f * headSize),
					doodleCenterX, doodleTop + (4 * headSize), paint);
		}
	}

	private void drawLeftHand(Canvas canvas) {
		if (game.getFailCount() >= 3) {
			canvas.drawLine(doodleCenterX, doodleTop + (2 * headSize),
					doodleCenterX - headSize, doodleTop + (3.5f * headSize),
					paint);
		}
	}

	private void drawRightHand(Canvas canvas) {
		if (game.getFailCount() >= 4) {
			canvas.drawLine(doodleCenterX, doodleTop + (2 * headSize),
					doodleCenterX + headSize, doodleTop + (3.5f * headSize),
					paint);
		}
	}

	private void drawLeftLeg(Canvas canvas) {
		if (game.getFailCount() >= 5) {
			canvas.drawLine(doodleCenterX, doodleTop + (4 * headSize),
					doodleCenterX - headSize, doodleTop + (5.5f * headSize),
					paint);
		}
	}

	private void drawRightLeg(Canvas canvas) {
		if (game.getFailCount() >= 6) {
			canvas.drawLine(doodleCenterX, doodleTop + (4 * headSize),
					doodleCenterX + headSize, doodleTop + (5.5f * headSize),
					paint);
		}
	}
}
