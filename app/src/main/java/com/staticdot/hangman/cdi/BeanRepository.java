package com.staticdot.hangman.cdi;

import java.util.HashMap;
import java.util.Map;

import com.staticdot.hangman.logic.Game;

/**
 * A place to hold static data for the game. Data can't be stored inside
 * Activities, since they get destroyed and re-created on every screen rotate.
 */
public class BeanRepository {

	private static Map<Class<?>, Object> beans = new HashMap<Class<?>, Object>();
	
	static {
		beans.put(Game.class, new Game());
	}

	private BeanRepository() {
	}
	
	public static <T> T getBean(Class<T> beanClass) {
		return (T) beans.get(beanClass);
	}
}
