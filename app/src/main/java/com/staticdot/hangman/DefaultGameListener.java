package com.staticdot.hangman;

import com.staticdot.hangman.cdi.BeanRepository;
import com.staticdot.hangman.logic.Game;
import com.staticdot.hangman.logic.GameListener;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;

class DefaultGameListener implements GameListener {

	private static final String TAG = DefaultGameListener.class.getSimpleName();
	
	private Context context;
	private MainGamePanel mainGamePanel;
	private final Game game = BeanRepository.getBean(Game.class);

	public DefaultGameListener(MainGamePanel mainGamePanel) {
        this.mainGamePanel = mainGamePanel;
        this.context = mainGamePanel.getContext();
	}

    public void gameStarted() {
        mainGamePanel.refreshGameState();
    }

	public void gameWon() {
		showDialog(context.getString(R.string.you_won));
	}

	public void gameLost() {
		showDialog(context.getString(R.string.you_lost_it_was) + " "
				+ game.getHiddenWord().reveal());
	}

	private void showDialog(String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(message)
				.setCancelable(true)
				.setPositiveButton(
						context.getString(R.string.play_again),
						playAgainListener)
				.setNegativeButton(context.getString(R.string.quit),
						playAgainListener);
		builder.show();
	}

	private final DialogInterface.OnClickListener playAgainListener = new DialogInterface.OnClickListener() {

		public void onClick(DialogInterface dialog, int which) {
			if (which == DialogInterface.BUTTON_POSITIVE) {
				Log.d(TAG, "play again");
				game.reset();
			} else if (which == DialogInterface.BUTTON_NEGATIVE) {
				Log.d(TAG, "quit");
                mainGamePanel.getExitListener().onExit();
			}
		}
	};
}
