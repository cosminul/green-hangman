package com.staticdot.hangman;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;

public class HangmanActivity extends Activity {

	private final static String TAG = HangmanActivity.class.getSimpleName();

	private MainGamePanel gamePanel;

    private ExitListener exitListener = new ExitListener() {

        public void onExit() {
            finish();
        }
    };

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);

		goFullscreen();

		gamePanel = new MainGamePanel(this);
        gamePanel.setExitListener(exitListener);
		setContentView(gamePanel);
		Log.d(TAG, "View added");
	}

	private void goFullscreen() {
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
	}

	@Override
	protected void onDestroy() {
		Log.d(TAG, "Destroying...");
		super.onDestroy();
	}

	@Override
	protected void onStop() {
		Log.d(TAG, "Stopping...");
		super.onStop();
	}

	@Override
	protected void onPause() {
		Log.d(TAG, "Pausing...");
		super.onPause();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (gamePanel.onKeyDown(keyCode, event)) {
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}
