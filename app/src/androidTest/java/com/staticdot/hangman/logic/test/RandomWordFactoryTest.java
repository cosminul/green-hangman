package com.staticdot.hangman.logic.test;

import com.staticdot.hangman.model.RandomWordFactory;
import com.staticdot.hangman.model.StringListSource;
import com.staticdot.hangman.model.WordFactory;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.easymock.EasyMock;
import org.easymock.IMocksControl;

import java.util.Arrays;
import java.util.List;

public class RandomWordFactoryTest extends TestCase {

    private static final List<String> WORDS = Arrays.asList("one", "two", "three");

    private WordFactory wordFactory;
    private StringListSource stringListSourceMock;

    private IMocksControl mocksControl = EasyMock.createStrictControl();

    @Override
    public void setUp() {
        stringListSourceMock = mocksControl.createMock(StringListSource.class);
    }

    public void testGetWord() {
        EasyMock.expect(stringListSourceMock.getStrings()).andReturn(WORDS);
        mocksControl.replay();

        wordFactory = new RandomWordFactory(stringListSourceMock);
        String word = wordFactory.getWord();

        mocksControl.verify();
        Assert.assertNotNull(word);
        Assert.assertTrue(WORDS.contains(word));
    }
}
