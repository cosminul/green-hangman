package com.staticdot.hangman.logic.test;

import junit.framework.Assert;
import junit.framework.TestCase;

import com.staticdot.hangman.logic.Game;
import com.staticdot.hangman.logic.GameListener;
import com.staticdot.hangman.model.WordFactory;

import org.easymock.EasyMock;
import org.easymock.IMocksControl;

public class GameTest extends TestCase {

	private Game game = new Game();
    private IMocksControl mocksControl = EasyMock.createStrictControl();
    private WordFactory wordFactoryMock;
    private GameListener gameListenerMock;

    private String initialWord;
    private char guessLetter;
    private int expectedFailCount;

    @Override
	public void setUp() {
        wordFactoryMock = mocksControl.createMock(WordFactory.class);
        gameListenerMock = mocksControl.createMock(GameListener.class);
        game.setWordFactory(wordFactoryMock);
        game.setListener(gameListenerMock);
	}

    @Override
    public void tearDown() {
    }
	
	public void testGuessSuccess() {
        initialWord = "abcde";
        guessLetter = 'c';
        expectedFailCount = 0;

        runGuessTest();
	}

    public void testGuessSuccessCaseInsensitive() {
        initialWord = "abcde";
        guessLetter = 'C';
        expectedFailCount = 0;

        runGuessTest();
    }

    public void testGuessFail() {
        initialWord = "abcde";
        guessLetter = 'z';
        expectedFailCount = 1;

        runGuessTest();
    }

    private void runGuessTest() {
        EasyMock.expect(wordFactoryMock.getWord()).andReturn(initialWord);
        mocksControl.replay();

        game.reset();

        game.guess(guessLetter);

        mocksControl.verify();
        Assert.assertEquals(expectedFailCount, game.getFailCount());
    }

    public void testGameWon() {
        EasyMock.expect(wordFactoryMock.getWord()).andReturn("abcde");
        gameListenerMock.gameWon();
        mocksControl.replay();

        game.reset();

        game.guess('a');
        game.guess('b');
        game.guess('c');
        game.guess('d');
        game.guess('e');

        mocksControl.verify();
        Assert.assertEquals(0, game.getFailCount());
    }

    public void testGameLost() {
        EasyMock.expect(wordFactoryMock.getWord()).andReturn("abcde");
        gameListenerMock.gameLost();
        mocksControl.replay();

        game.reset();

        game.guess('z');
        game.guess('y');
        game.guess('x');
        game.guess('w');
        game.guess('v');
        game.guess('u');

        mocksControl.verify();
        Assert.assertEquals(6, game.getFailCount());
    }
}
